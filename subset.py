import time

def subset(n):
    S=list()
    for i in range(1,n+1):
        S.append(i)
    return S


def array_sum(a):
    sum=0
    for i in a:
        sum += i
    return sum


def error_cal(a,total):
    E=abs(array_sum(a) - total)
#     print("Error is {}: ".format(E))
    return E

def swap(source,destination,c):
    destination.append(c)
    return source,destination


def smallest(a):
    a.sort()
#     print("Min value in source {}".format(a[0]))
    return a[0]
n = input("Enter Any integer : ")
start = time.time()
n= int(n) 
total=(n*(n+1))/6
# if abs(total) != total:
#     continue
S1=subset(n)
S2=[]
S3=[]
for index,i in enumerate(S1):
#     print("value of I is {}".format(i))
    S1,S2=swap(S1,S2,i)
    temp_source = list(set(S1)-set(S2))
    if array_sum(S2)==total:
        S1 = temp_source
        break
    if error_cal(S2,total)<smallest(temp_source):
        S2.remove(i)
#     print("S2 sum {}".format(array_sum(S2)))
#     print("value after iteration {} ".format(i))
#     print("*****************")
# print("*****************")
# print(S1)
# print(S2)
# print("*****************")

for index,i in enumerate(S1):
#     print("value of I is {}".format(i))
    S1,S3=swap(S1,S3,i)
    temp_source = list(set(S1)-set(S3))
    if array_sum(S3)==total:
        S1 = temp_source
        break
    if error_cal(S3,total)<smallest(temp_source):
        S3.remove(i)
#     print("S3 sum {}".format(array_sum(S3)))
#     print("value after iteration {} ".format(i))
#     print("*****************")
# print("*****************")
# print(S1)
# print(S3)
# print("*****************")
if (array_sum(S1)==array_sum(S2)) and (array_sum(S2)==array_sum(S3)):
#             print("Final Result") 
        print("subset of {}: {} , {} , {}".format(n,S1,S2,S3))
#             print ("YES")

else:

     print("subset cannot be formed of {}".format(n))
print("Time taken: {}".format(time.time()-start))

